<!DOCTYPE html>
<html lang='en'>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="color_form.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>

    <title>Register</title>
</head>

<body>
    <?php
    session_start();
    if (!empty($_POST['register'])) {
        header("Location: ./danhsach.php ");
    }
    ?>
    <form method="post" enctype="multipart/form-data" action="">
	<div class="form_center">	
	    <table class="table_form">
		<tr><td>
			<div class="form_td">Họ và tên<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div> </td>
					<td ><p class='input-form'>
                    <?php
                    echo  $_SESSION["name"];
                    ?>
                </p></td>
				</tr>
				<tr>
				<td><div class="form_td">Giới tính<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
				<td>
                <div>
                        <?php echo $_SESSION['gender']; ?>
                    </div>
					</td>
</tr>
<td><div class="form_td">Phân Khoa<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
					<td>
					<div>
                        <?php echo $_SESSION['depart']; ?>
                    </div>
				</tr>
				

				<td><div class="form_td">Ngày sinh<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
                    <td>
                    <div>
                        <?php echo $_SESSION['birth']; ?>
                    </div>
					</td>
				</tr>
				<tr>
				<td><div class="form_td">Địa chỉ<sup class="sup"></sup></div>
					<td ><p class='input-form'>
                    <div>
                        <?php echo $_SESSION['address']; ?>
                    </div>
				</tr>

				<td><div for="avatar" class="form_td">Hình ảnh<sup class="sup"> </sup></div>
				<td> <div>
                        <?php echo '<img src="' . $_SESSION["image"] . '" alt="Avatar" width="150" height="100">'; ?></br>
                    </div>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">

						<input class="form_submit" type="submit" method="POST" name="register" value="Xác nhận">
					</td>
				</tr>


				</form>
</table>


    <script type="text/javascript">
        $(function() {
            $('#datepicker').datepicker();
        });
    </script>
</body>

</html>
